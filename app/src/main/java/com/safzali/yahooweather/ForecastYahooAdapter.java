package com.safzali.yahooweather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.safzali.utils.Constant;
import com.safzali.yahooweather.yahoomodels.Forecast;
import com.safzali.yahooweather.yahoomodels.YahooModel;

import java.util.List;

public class ForecastYahooAdapter extends BaseAdapter {
    private Context mContext;
    private List<Forecast> forecasts;

    ForecastYahooAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.item_forecasts_list, parent, false);
        TextView day = v.findViewById(R.id.day);
        TextView date = v.findViewById(R.id.date);
        TextView hige = v.findViewById(R.id.high);
        TextView low = v.findViewById(R.id.low);
        TextView text = v.findViewById(R.id.text);
        ImageView icon = v.findViewById(R.id.icon);
        Forecast currentData = forecasts.get(position);
        day.setText(currentData.getDay());
        date.setText(currentData.getDate());
        hige.setText(currentData.getHigh());
        low.setText(currentData.getLow());
        text.setText(currentData.getText());
        String img = "";

        if (currentData.getText().toLowerCase().contains("showers")) {
            img = Constant.imgIconSetterShowers;
        } else if (currentData.getText().toLowerCase().contains("sunny")) {
            img = Constant.imgIconSetterSunny;
        } else if (currentData.getText().toLowerCase().contains("partly cloudy")) {
            img = Constant.imgIconSetterCloudy;
        }
        Glide.with(mContext).load(img).into(icon);

        return v;

    }
}
