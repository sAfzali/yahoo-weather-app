package com.safzali.yahooweather;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.safzali.utils.BaseActivity;
import com.safzali.utils.Constant;
import com.safzali.utils.PublicMethods;
import com.safzali.yahooweather.yahoomodels.YahooModel;

import cz.msebera.android.httpclient.Header;

public class YahooWeatherActivity extends BaseActivity {
    EditText cityName;
    ListView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo_weather);
        bind();
        checkStatusInternet();
    }

    private void checkStatusInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        } else
            PublicMethods.toast(mContext, Constant.checkInternetConnection);
    }

    private void bind() {
        cityName = findViewById(R.id.city_name);
        result = findViewById(R.id.result);
        findViewById(R.id.show_weather).setOnClickListener(v -> {
            goToWeatherByYahoo(cityName.getText().toString());
        });
    }

    private void goToWeatherByYahoo(String city) {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = Constant.urlPieceOne + city + Constant.urlPieceTwo;
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                PublicMethods.toast(mContext, Constant.onFailureMsg);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                parseAndLoadData(responseString);
            }
        });
    }

    private void parseAndLoadData(String response) {
        try {
            Gson gson = new Gson();
            YahooModel yahoo = gson.fromJson(response, YahooModel.class);
            if (yahoo.getQuery().getCount() == 1) {
                ForecastYahooAdapter yahooAdapter = new ForecastYahooAdapter(mContext, yahoo.getQuery().getResults()
                        .getChannel().getItem().getForecast());
                result.setAdapter(yahooAdapter);
            } else {
                PublicMethods.toast(mContext, Constant.tryCatchError);
            }

        } catch (Exception e) {
            PublicMethods.toast(mContext, Constant.exception);
        }
    }
}
